package com.example.adrian.quiz;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;


public class Quiz2 extends Activity {
    public ArrayList<Question> quiz2 = new ArrayList<Question>();
    Bundle data;
    Intent intent;
    int numberOfQuestion;
    TextView question;
    TextView questionContent;
    ImageView stadiumPhoto;
    Button answer1;
    Button answer2;
    Button answer3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        createQuiz();
        setContentView(R.layout.activity_quiz1);

        question = (TextView)findViewById(R.id.textView3);
        questionContent = (TextView)findViewById(R.id.textView4);
        stadiumPhoto = (ImageView)findViewById(R.id.imageView4);
        answer1 = (Button)findViewById(R.id.button2);
        answer2 = (Button)findViewById(R.id.button3);
        answer3 = (Button)findViewById(R.id.button4);


        question.setText("Pytanie 1");
        questionContent.setText(quiz2.get(0).stadiumQuestion);
        answer1.setText(quiz2.get(0).answer1);
        answer2.setText(quiz2.get(0).answer2);
        answer3.setText(quiz2.get(0).answer3);
        stadiumPhoto.setImageResource(R.drawable.lm);
        numberOfQuestion = 1;


        intent = new Intent(Quiz2.this, ResultQuiz2.class);
        data = new Bundle();
    }

    public void onClick(View view){
        switch (view.getId()){
            case R.id.button2:
                if(numberOfQuestion == 1) {
                    question.setText("Pytanie 2");
                    questionContent.setText(quiz2.get(1).stadiumQuestion);
                    answer1.setText(quiz2.get(1).answer1);
                    answer2.setText(quiz2.get(1).answer2);
                    answer3.setText(quiz2.get(1).answer3);
                    stadiumPhoto.setImageResource(R.drawable.le);
                    numberOfQuestion = 2;
                    data.putInt("chooseAnswer1", R.id.button2);
                    intent.putExtras(data);
                }
                else if (numberOfQuestion==2){
                    question.setText("Pytanie 3");
                    questionContent.setText(quiz2.get(2).stadiumQuestion);
                    answer1.setText(quiz2.get(2).answer1);
                    answer2.setText(quiz2.get(2).answer2);
                    answer3.setText(quiz2.get(2).answer3);
                    stadiumPhoto.setImageResource(R.drawable.supercup);
                    numberOfQuestion = 3;
                    data.putInt("chooseAnswer2", R.id.button2);
                    intent.putExtras(data);
                }
                else{
                    data.putInt("chooseAnswer3", R.id.button2);
                    intent.putExtras(data);
                    startActivity(intent);
                }
                break;
            case R.id.button3:
                if(numberOfQuestion == 1) {
                    question.setText("Pytanie 2");
                    questionContent.setText(quiz2.get(1).stadiumQuestion);
                    answer1.setText(quiz2.get(1).answer1);
                    answer2.setText(quiz2.get(1).answer2);
                    answer3.setText(quiz2.get(1).answer3);
                    stadiumPhoto.setImageResource(R.drawable.le);
                    numberOfQuestion = 2;
                    data.putInt("chooseAnswer1", R.id.button3);
                    intent.putExtras(data);
                }
                else if (numberOfQuestion==2){
                    question.setText("Pytanie 3");
                    questionContent.setText(quiz2.get(2).stadiumQuestion);
                    answer1.setText(quiz2.get(2).answer1);
                    answer2.setText(quiz2.get(2).answer2);
                    answer3.setText(quiz2.get(2).answer3);
                    stadiumPhoto.setImageResource(R.drawable.supercup);
                    numberOfQuestion = 3;
                    data.putInt("chooseAnswer2", R.id.button3);
                    intent.putExtras(data);
                }
                else{
                    data.putInt("chooseAnswer3", R.id.button3);
                    intent.putExtras(data);
                    startActivity(intent);
                }
                break;
            case R.id.button4:
                if(numberOfQuestion == 1) {
                    question.setText("Pytanie 2");
                    questionContent.setText(quiz2.get(1).stadiumQuestion);
                    answer1.setText(quiz2.get(1).answer1);
                    answer2.setText(quiz2.get(1).answer2);
                    answer3.setText(quiz2.get(1).answer3);
                    stadiumPhoto.setImageResource(R.drawable.le);
                    numberOfQuestion = 2;
                    data.putInt("chooseAnswer1", R.id.button4);
                    intent.putExtras(data);
                }
                else if (numberOfQuestion==2){
                    question.setText("Pytanie 3");
                    questionContent.setText(quiz2.get(2).stadiumQuestion);
                    answer1.setText(quiz2.get(2).answer1);
                    answer2.setText(quiz2.get(2).answer2);
                    answer3.setText(quiz2.get(2).answer3);
                    stadiumPhoto.setImageResource(R.drawable.supercup);
                    numberOfQuestion = 3;
                    data.putInt("chooseAnswer2", R.id.button4);
                    intent.putExtras(data);
                }
                else{
                    data.putInt("chooseAnswer3", R.id.button4);
                    intent.putExtras(data);
                    startActivity(intent);
                }
                break;
        }

    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_quiz1, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void createQuiz(){
        Question question1 = new Question();
        question1.stadiumPhoto = "R.drawable.lm";
        question1.stadiumQuestion = "Kto wygrał najwięcej razy Ligę Mistrzów?";
        question1.answer1 = "FC Barcelona";
        question1.answer2 = "Bayern Monachium";
        question1.answer3 = "Real Madryt";
        question1.goodAnswer = 3;
        quiz2.add(question1);

        Question question2 = new Question();
        question2.stadiumPhoto = "R.drawable.le";
        question2.stadiumQuestion = "Kto jest obecnym zwycięzcą Ligi Europy?";
        question2.answer1 = "Atlético Madryt";
        question2.answer2 = "Sevilla FC";
        question2.answer3 = "Inter Mediolan";
        question2.goodAnswer = 2;
        quiz2.add(question2);

        Question question3 = new Question();
        question3.stadiumPhoto = "R.drawable.supercup";
        question3.stadiumQuestion = "Kto gra z kim w Superpucharze Europy?";
        question3.answer1 = "Nie istnieje coś takiego";
        question3.answer2 = "Zwycięzca LM i LE";
        question3.answer3 = "Zwycięzca i finalista LM";
        question2.goodAnswer = 1;
        quiz2.add(question3);
    }

}
