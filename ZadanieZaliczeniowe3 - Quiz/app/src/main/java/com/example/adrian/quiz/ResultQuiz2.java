package com.example.adrian.quiz;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;


public class ResultQuiz2 extends Activity {
    Bundle data;
    int answer1, answer2, answer3;
    Button start;
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result_quiz2);

        intent = getIntent();
        data = new Bundle();
        data = intent.getExtras();
        answer1 = data.getInt("chooseAnswer1", 0);
        answer2 = data.getInt("chooseAnswer2", 0);
        answer3 = data.getInt("chooseAnswer3", 0);

        start = (Button)findViewById(R.id.button);
        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ResultQuiz2.this, ResultQuiz2Detail.class);
                data.putInt("chooseAnswer1", answer1);
                data.putInt("chooseAnswer2", answer2);
                data.putInt("chooseAnswer3", answer3);
                intent.putExtras(data);
                startActivity(intent);
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_results_quiz1, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
