package com.example.adrian.quiz;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;


public class Quiz1 extends Activity {
    public ArrayList<Question> quiz1 = new ArrayList<Question>();
    Bundle data;
    Intent intent;
    int numberOfQuestion;
    TextView question;
    TextView questionContent;
    ImageView stadiumPhoto;
    Button answer1;
    Button answer2;
    Button answer3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        createQuiz();
        setContentView(R.layout.activity_quiz1);

        question = (TextView)findViewById(R.id.textView3);
        questionContent = (TextView)findViewById(R.id.textView4);
        stadiumPhoto = (ImageView)findViewById(R.id.imageView4);
        answer1 = (Button)findViewById(R.id.button2);
        answer2 = (Button)findViewById(R.id.button3);
        answer3 = (Button)findViewById(R.id.button4);

        question.setText("Pytanie 1");
        questionContent.setText(quiz1.get(0).stadiumQuestion);
        answer1.setText(quiz1.get(0).answer1);
        answer2.setText(quiz1.get(0).answer2);
        answer3.setText(quiz1.get(0).answer3);
        stadiumPhoto.setImageResource(R.drawable.wroclaw);
        numberOfQuestion = 1;

        intent = new Intent(Quiz1.this, ResultsQuiz1.class);
        data = new Bundle();
    }

    public void onClick(View view){
        switch (view.getId()){
            case R.id.button2:
                if(numberOfQuestion == 1) {
                    question.setText("Pytanie 2");
                    questionContent.setText(quiz1.get(1).stadiumQuestion);
                    answer1.setText(quiz1.get(1).answer1);
                    answer2.setText(quiz1.get(1).answer2);
                    answer3.setText(quiz1.get(1).answer3);
                    stadiumPhoto.setImageResource(R.drawable.gdansk);
                    numberOfQuestion = 2;
                    data.putInt("chooseAnswer1", R.id.button2);
                    intent.putExtras(data);
                }
                else if (numberOfQuestion==2){
                    question.setText("Pytanie 3");
                    questionContent.setText(quiz1.get(2).stadiumQuestion);
                    answer1.setText(quiz1.get(2).answer1);
                    answer2.setText(quiz1.get(2).answer2);
                    answer3.setText(quiz1.get(2).answer3);
                    stadiumPhoto.setImageResource(R.drawable.narodowy);
                    numberOfQuestion = 3;
                    data.putInt("chooseAnswer2", R.id.button2);
                    intent.putExtras(data);
                }
                else{
                    data.putInt("chooseAnswer3", R.id.button2);
                    intent.putExtras(data);
                    startActivity(intent);
                }
                break;
            case R.id.button3:
                if(numberOfQuestion == 1) {
                    question.setText("Pytanie 2");
                    questionContent.setText(quiz1.get(1).stadiumQuestion);
                    answer1.setText(quiz1.get(1).answer1);
                    answer2.setText(quiz1.get(1).answer2);
                    answer3.setText(quiz1.get(1).answer3);
                    stadiumPhoto.setImageResource(R.drawable.gdansk);
                    numberOfQuestion = 2;
                    data.putInt("chooseAnswer1", R.id.button3);
                    intent.putExtras(data);
                }
                else if (numberOfQuestion==2){
                    question.setText("Pytanie 3");
                    questionContent.setText(quiz1.get(2).stadiumQuestion);
                    answer1.setText(quiz1.get(2).answer1);
                    answer2.setText(quiz1.get(2).answer2);
                    answer3.setText(quiz1.get(2).answer3);
                    stadiumPhoto.setImageResource(R.drawable.narodowy);
                    numberOfQuestion = 3;
                    data.putInt("chooseAnswer2", R.id.button3);
                    intent.putExtras(data);
                }
                else{
                    data.putInt("chooseAnswer3", R.id.button3);
                    intent.putExtras(data);
                    startActivity(intent);
                }
                break;
            case R.id.button4:
                if(numberOfQuestion == 1) {
                    question.setText("Pytanie 2");
                    questionContent.setText(quiz1.get(1).stadiumQuestion);
                    answer1.setText(quiz1.get(1).answer1);
                    answer2.setText(quiz1.get(1).answer2);
                    answer3.setText(quiz1.get(1).answer3);
                    stadiumPhoto.setImageResource(R.drawable.gdansk);
                    numberOfQuestion = 2;
                    data.putInt("chooseAnswer1", R.id.button4);
                    intent.putExtras(data);
                }
                else if (numberOfQuestion==2){
                    question.setText("Pytanie 3");
                    questionContent.setText(quiz1.get(2).stadiumQuestion);
                    answer1.setText(quiz1.get(2).answer1);
                    answer2.setText(quiz1.get(2).answer2);
                    answer3.setText(quiz1.get(2).answer3);
                    stadiumPhoto.setImageResource(R.drawable.narodowy);
                    numberOfQuestion = 3;
                    data.putInt("chooseAnswer2", R.id.button4);
                    intent.putExtras(data);
                }
                else{
                    data.putInt("chooseAnswer3", R.id.button4);
                    intent.putExtras(data);
                    startActivity(intent);
                }
                break;
        }

    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_quiz1, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void createQuiz(){
        Question question1 = new Question();
        question1.stadiumPhoto = "R.drawable.wroclaw";
        question1.stadiumQuestion = "Gdzie znajduje się poniższy stadion?";
        question1.answer1 = "we Wrocławiu";
        question1.answer2 = "w Gdańsku";
        question1.answer3 = "w Warszawie";
        question1.goodAnswer = 1;
        quiz1.add(question1);

        Question question2 = new Question();
        question2.stadiumPhoto = "R.drawable.gdansk";
        question2.stadiumQuestion = "Jaką nazwę nosi poniższy stadion?";
        question2.answer1 = "Stadion Gdańsk";
        question2.answer2 = "PGE Arena Gdańsk";
        question2.answer3 = "Arena Gdańsk";
        question2.goodAnswer = 2;
        quiz1.add(question2);

        Question question3 = new Question();
        question3.stadiumPhoto = "R.drawable.narodowy";
        question3.stadiumQuestion = "Kiedy otwarto poniższy stadion?";
        question3.answer1 = "29 stycznia 2012";
        question3.answer2 = "29 marca 2012";
        question3.answer3 = "29 kwietnia 2012";
        question2.goodAnswer = 1;
        quiz1.add(question3);
    }

}
