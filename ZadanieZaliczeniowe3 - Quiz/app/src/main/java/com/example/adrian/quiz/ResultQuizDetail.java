package com.example.adrian.quiz;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class ResultQuizDetail extends Activity {
    Quiz1 firstQuiz = new Quiz1();
    TextView question;
    TextView questionContent;
    ImageView stadiumPhoto;
    Button answer1;
    Button answer2;
    Button answer3;
    Button go;
    int numberOfQuestion;
    int answerFirst;
    int answerSecond;
    int answerThird;
    TextView textView6, textView7, textView8, textView9, textView10, textView11;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result_quiz_detail);
        firstQuiz.createQuiz();

        question = (TextView)findViewById(R.id.textView3);
        questionContent = (TextView)findViewById(R.id.textView4);
        stadiumPhoto = (ImageView)findViewById(R.id.imageView4);
        answer1 = (Button)findViewById(R.id.button2);
        answer2 = (Button)findViewById(R.id.button3);
        answer3 = (Button)findViewById(R.id.button4);
        go = (Button)findViewById(R.id.button5);
        numberOfQuestion = 1;

        question.setText("Pytanie 1");
        questionContent.setText(firstQuiz.quiz1.get(0).stadiumQuestion);
        answer1.setText(firstQuiz.quiz1.get(0).answer1);
        answer2.setText(firstQuiz.quiz1.get(0).answer2);
        answer3.setText(firstQuiz.quiz1.get(0).answer3);
        stadiumPhoto.setImageResource(R.drawable.wroclaw);

        Intent intent = getIntent();
        Bundle data = new Bundle();
        data = intent.getExtras();
        answerFirst = data.getInt("chooseAnswer1", 0);
        answerSecond = data.getInt("chooseAnswer2", 0);
        answerThird = data.getInt("chooseAnswer3", 0);

        textView6 = (TextView)findViewById(R.id.textView6);
        textView7 = (TextView)findViewById(R.id.textView7);
        textView8 = (TextView)findViewById(R.id.textView8);
        textView9 = (TextView)findViewById(R.id.textView9);
        textView10 = (TextView)findViewById(R.id.textView10);
        textView11 = (TextView)findViewById(R.id.textView11);
        textView6.setVisibility(View.VISIBLE);
        textView7.setVisibility(View.INVISIBLE);;
        textView8.setVisibility(View.INVISIBLE);
        textView9.setVisibility(View.INVISIBLE);
        textView10.setVisibility(View.INVISIBLE);
        textView11.setVisibility(View.INVISIBLE);

        if(answerFirst==2131492937)
        {
            textView7.setVisibility(View.VISIBLE);
        }
        else if(answerFirst==2131492938)
        {
            textView9.setVisibility(View.VISIBLE);
        }
        else if(answerFirst==2131492939)
        {
            textView11.setVisibility(View.VISIBLE);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_result_quiz_detail, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onClick(View view){
        switch (view.getId()) {
            case R.id.button5:
                if (numberOfQuestion == 1) {
                    question.setText("Pytanie 2");
                    questionContent.setText(firstQuiz.quiz1.get(1).stadiumQuestion);
                    answer1.setText(firstQuiz.quiz1.get(1).answer1);
                    answer2.setText(firstQuiz.quiz1.get(1).answer2);
                    answer3.setText(firstQuiz.quiz1.get(1).answer3);
                    stadiumPhoto.setImageResource(R.drawable.gdansk);
                    numberOfQuestion = 2;

                    if(numberOfQuestion==2)
                    {
                        textView6.setVisibility(View.INVISIBLE);
                        textView8.setVisibility(View.VISIBLE);
                        textView10.setVisibility(View.INVISIBLE);
                        if(answerSecond==2131492937)
                        {
                            textView7.setVisibility(View.VISIBLE);
                            textView9.setVisibility(View.INVISIBLE);
                            textView11.setVisibility(View.INVISIBLE);
                        }
                        else if(answerSecond==2131492938)
                        {
                            textView7.setVisibility(View.INVISIBLE);
                            textView9.setVisibility(View.VISIBLE);
                            textView11.setVisibility(View.INVISIBLE);
                        }
                        else if(answerSecond==2131492939)
                        {
                            textView7.setVisibility(View.INVISIBLE);
                            textView9.setVisibility(View.INVISIBLE);
                            textView11.setVisibility(View.VISIBLE);
                        }
                    }

                } else if (numberOfQuestion == 2) {
                    question.setText("Pytanie 3");
                    questionContent.setText(firstQuiz.quiz1.get(2).stadiumQuestion);
                    answer1.setText(firstQuiz.quiz1.get(2).answer1);
                    answer2.setText(firstQuiz.quiz1.get(2).answer2);
                    answer3.setText(firstQuiz.quiz1.get(2).answer3);
                    stadiumPhoto.setImageResource(R.drawable.narodowy);
                    numberOfQuestion = 3;

                    if(numberOfQuestion==3)
                    {
                        textView6.setVisibility(View.VISIBLE);
                        textView8.setVisibility(View.INVISIBLE);
                        textView10.setVisibility(View.INVISIBLE);
                        if(answerThird==2131492937)
                        {
                            textView7.setVisibility(View.VISIBLE);
                            textView9.setVisibility(View.INVISIBLE);
                            textView11.setVisibility(View.INVISIBLE);
                        }
                        else if(answerThird==2131492938)
                        {
                            textView7.setVisibility(View.INVISIBLE);
                            textView9.setVisibility(View.VISIBLE);
                            textView11.setVisibility(View.INVISIBLE);
                        }
                        else if(answerThird==2131492939)
                        {
                            textView7.setVisibility(View.INVISIBLE);
                            textView9.setVisibility(View.INVISIBLE);
                            textView11.setVisibility(View.VISIBLE);
                        }
                     }

                    go.setText("Następny QUIZ");

                } else {
                    Intent intent = new Intent(ResultQuizDetail.this, SecondQuiz.class);
                    startActivity(intent);
                }
                break;
        }
    }


}
