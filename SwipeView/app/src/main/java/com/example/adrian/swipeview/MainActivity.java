package com.example.adrian.swipeview;

import java.util.Locale;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.FragmentPagerAdapter;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.text.Editable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {


    SectionsPagerAdapter mSectionsPagerAdapter;

    ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager(), getApplicationContext());

        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(mSectionsPagerAdapter);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }



    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        String[] family;
        String[] familyPhones;
        String[] familyDescription;

        public SectionsPagerAdapter(FragmentManager fm, Context applicationContext) {
            super(fm);

            Resources resources = applicationContext.getResources();
            family = resources.getStringArray(R.array.family);
            familyDescription = resources.getStringArray(R.array.family_decription);
            familyPhones = resources.getStringArray(R.array.family_contactphone);
        }

        @Override
        public Fragment getItem(int position) {
            Bundle bundle = new Bundle();
            bundle.putString(PlaceholderFragment.DescriptionKey, familyDescription[position]);
            bundle.putInt(PlaceholderFragment.ImageIdKey, getFamilyImageId(position));
            bundle.putString(PlaceholderFragment.PhoneKey, familyPhones[position]);

            PlaceholderFragment familyFragment = new PlaceholderFragment();
            familyFragment.setArguments(bundle);

            return familyFragment;
        }

        @Override
        public int getCount() {
            return family.length;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return family[position];
        }

        private int getFamilyImageId (int position){
            int id = 0;
            switch(position){
                case 0:
                    id = R.mipmap.adrian;
                    break;
                case 1:
                    id = R.mipmap.ewa;
                    break;
                case 2:
                    id = R.mipmap.ic_launcher;
                    break;
            }
            return id;
        }
    }


    public static class PlaceholderFragment extends Fragment {

        public static final String ImageIdKey = "imagekey";
        public static final String DescriptionKey = "descriptionkey";
        public static final String PhoneKey = "phonekey";


        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);

            Bundle bundle = getArguments();

            if (bundle != null){
                int imageID = bundle.getInt(ImageIdKey);
                String description = bundle.getString(DescriptionKey);
                String phone = bundle.getString(PhoneKey);

                setValues(rootView, imageID, description, phone);
            }

            return rootView;
        }

        private void setValues(View view, int imageID, String description, String phone){
            ImageView imageView = (ImageView) view.findViewById(R.id.imageView);
            imageView.setImageResource(imageID);

            final TextView textView = (TextView) view.findViewById(R.id.textView);
            textView.setText(description);

            final TextView textView1 = (TextView) view.findViewById(R.id.textView2);
            textView1.setText(phone);

            textView1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String number = textView1.getText().toString();
                    Intent call = new Intent(Intent.ACTION_CALL);
                    call.setData(Uri.parse("tel:"+number));
                    startActivity(call);
                }
            });

        }

    }

}
