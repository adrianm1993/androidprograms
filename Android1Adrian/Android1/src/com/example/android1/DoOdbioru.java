package com.example.android1;


import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class DoOdbioru extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_do_odbioru);

		if (savedInstanceState == null) {
			getFragmentManager().beginTransaction()
					.add(R.id.container, new PlaceholderFragment()).commit();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.do_odbioru, menu);
		
		TextView l1 = (TextView) findViewById(R.id.textView3);
	   	TextView l2 = (TextView) findViewById(R.id.textView4);
	   	Intent cel = getIntent();
	    Bundle dane = new Bundle();
	   			
	   	dane = cel.getExtras();
	   	float zm1 = dane.getFloat("liczba1");
	   	float zm2 = dane.getFloat("liczba2");
	   	l1.setText(""+zm1);
	   	l2.setText(""+zm2);
	   	
	   	TextView l3 = (TextView) findViewById(R.id.textView6);
	   	final float wynik = zm1+zm2;
	   	l3.setText(""+wynik);
	   	
	   	
	   	
	   	Button przycisk = (Button)findViewById(R.id.button1);
	   	przycisk.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
			 	Intent cel1 = getIntent();
			    Bundle dane1 = new Bundle();
				dane1.putFloat("wynik", wynik);
			 	cel1.putExtras(dane1);
			   	setResult(Activity.RESULT_OK, cel1);
			   	finish();
				
			}
	   		
	   	});
		
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_do_odbioru,
					container, false);
			return rootView;
		}
	}

}
