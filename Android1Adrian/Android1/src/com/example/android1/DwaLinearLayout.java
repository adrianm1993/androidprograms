package com.example.android1;


import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

public class DwaLinearLayout extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_dwa_linear_layout);

		if (savedInstanceState == null) {
			getFragmentManager().beginTransaction()
					.add(R.id.container, new PlaceholderFragment()).commit();
		}
	}
	
	public void poKliknieciu(View widok){
		Context kontekst = getApplicationContext();
		
		switch(widok.getId()){
		case R.id.button1:
			EditText tb1 = (EditText)findViewById(R.id.editText1);
			EditText tb3 = (EditText)findViewById(R.id.editText3);
			CheckBox tb4 = (CheckBox)findViewById(R.id.checkBox1);
			CheckBox tb5 = (CheckBox)findViewById(R.id.checkBox2);
			CheckBox tb6 = (CheckBox)findViewById(R.id.checkBox3);
			String tekst = "Podsumowanie: " + tb1.getText().toString() + ". ";
			tekst += tb3.getText().toString() + ". Wybrane opcje:";
			if(tb4.isChecked()) tekst += tb4.getText().toString() + ". ";
			if(tb5.isChecked()) tekst += tb5.getText().toString() + ". ";
			if(tb6.isChecked()) tekst += tb6.getText().toString() + ". ";
			Toast toast1 = Toast.makeText(kontekst,tekst , Toast.LENGTH_SHORT);
			toast1.show();
			break;
		}
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.dwa_linear_layout, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(
					R.layout.fragment_dwa_linear_layout, container, false);
			return rootView;
		}
	}

}
