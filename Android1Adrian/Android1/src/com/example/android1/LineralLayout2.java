package com.example.android1;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import android.widget.ToggleButton;

public class LineralLayout2 extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_lineral_layout2);

		if (savedInstanceState == null) {
			getFragmentManager().beginTransaction()
					.add(R.id.container, new PlaceholderFragment()).commit();
		}
	}
	
	public void poKliknieciu(View widok){
		Context kontekst = getApplicationContext();
		ToggleButton tb1 = (ToggleButton)findViewById(R.id.toggleButton1);
		ToggleButton tb2 = (ToggleButton)findViewById(R.id.toggleButton2);
		ToggleButton tb3 = (ToggleButton)findViewById(R.id.toggleButton3);
		ToggleButton tb4 = (ToggleButton)findViewById(R.id.toggleButton4);
		int pom = 0;
		if(tb1.isChecked()) pom++;
		if(tb2.isChecked()) pom++;
		if(tb3.isChecked()) pom++;
		if(tb4.isChecked()) pom++;
		
		switch(widok.getId()){
		case R.id.button1:
			Toast toast1 = Toast.makeText(kontekst, "Liczba włączonych przycisków:" + pom + ". Liczba wyłączonych:" + (4-pom) + ". ", Toast.LENGTH_SHORT);
			toast1.show();
			break;
		}
		
	}
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.lineral_layout2, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_lineral_layout2,
					container, false);
			return rootView;
		}
	}

}
