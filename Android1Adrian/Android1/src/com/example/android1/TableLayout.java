package com.example.android1;


import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.Toast;

public class TableLayout extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_table_layout);

		if (savedInstanceState == null) {
			getFragmentManager().beginTransaction()
					.add(R.id.container, new PlaceholderFragment()).commit();
		}
	}
	
	public void poKliknieciu(View widok){
		Context kontekst = getApplicationContext();
		EditText miasto = (EditText) findViewById(R.id.editText1);
		EditText wojewodztwo = (EditText) findViewById(R.id.editText2);
		SeekBar seekbar = (SeekBar) findViewById(R.id.seekBar1);
		
		switch(widok.getId()){
		case R.id.button1:
			Toast toast1 = Toast.makeText(kontekst, "Wpisa�e� miasto: " + miasto.getText().toString() + ". ", Toast.LENGTH_LONG);
			toast1.show();
			break;
		case R.id.button2:
			Toast toast2 = Toast.makeText(kontekst, "Wpisa�e� wojew�dztwo: " + wojewodztwo.getText().toString() + ". ", Toast.LENGTH_LONG);
			toast2.show();
			break;
		case R.id.button4:
			Toast toast4 = Toast.makeText(kontekst, "Wybra�e� liczb�: " + seekbar.getProgress() + ". ", Toast.LENGTH_LONG);
			toast4.show();
			break;
    	case R.id.button3:
		    String tekst = "Podsumowanie: " + miasto.getText().toString() + ". ";
		    tekst += "Wojew�dztwo " + wojewodztwo.getText().toString() + ". ";
		    CheckBox checkbox = (CheckBox)findViewById(R.id.checkBox1);
		    if(checkbox.isChecked())
		    	tekst += "Mieszkaniec miasta. ";
		    else 
		    	tekst += "Osoba nie mieszka w tym mie�cie. ";
		    CheckBox checkbox2 = (CheckBox)findViewById(R.id.checkBox2);
		    if(checkbox2.isChecked())
		    	tekst += "Obywatel Polski. ";
		    else
		    	tekst += "Obywatel innego kraju. ";
		    tekst += "Wybrana liczba:" + seekbar.getProgress() + ". ";
			Toast toast3 = Toast.makeText(kontekst, tekst, Toast.LENGTH_LONG);
			toast3.show();
    		break;
		}
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.table_layout, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_table_layout,
					container, false);
			return rootView;
		}
	}

}
