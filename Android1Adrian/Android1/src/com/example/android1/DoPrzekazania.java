package com.example.android1;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

public class DoPrzekazania extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_do_przekazania);

		if (savedInstanceState == null) {
			getFragmentManager().beginTransaction()
					.add(R.id.container, new PlaceholderFragment()).commit();
		}
	}

	public void poKliknieciu(View widok){
		//	Context kontekst = getApplicationContext();
			EditText liczba1 = (EditText) findViewById(R.id.editText1);
			EditText liczba2 = (EditText) findViewById(R.id.editText2);
			Intent cel = null;

			switch(widok.getId()){
			case R.id.button1:
				cel = new Intent(this, DoOdbioru.class);
				Bundle dane = new Bundle();
				dane.putFloat("liczba1", Float.parseFloat(liczba1.getText().toString()));
				dane.putFloat("liczba2", Float.parseFloat(liczba2.getText().toString()));
				cel.putExtras(dane);
				startActivityForResult(cel,123);
				break;
			}
		}
	
	@Override
	protected void onActivityResult(int reqID, int resC, Intent ii){
		if(resC==Activity.RESULT_OK && reqID == 123){
			Bundle dane = new Bundle();
			dane = ii.getExtras();
			Float wynik = dane.getFloat("wynik");
		 	TextView l1 = (TextView) findViewById(R.id.textView2);
		 	l1.setText("Otrzymany wynik:" + wynik);
		}
		else{
		}
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.do_przekazania, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_do_przekazania,
					container, false);
			return rootView;
		}
	}

}
