package com.example.android1;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

public class RelativeLayout extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_relative_layout);

		if (savedInstanceState == null) {
			getFragmentManager().beginTransaction()
					.add(R.id.container, new PlaceholderFragment()).commit();
		}
	}
	
	public void poKliknieciu(View widok){
		Context kontekst = getApplicationContext();
		EditText pole1 = (EditText) findViewById(R.id.editText1);
		RadioGroup rg = (RadioGroup) findViewById(R.id.radioGroup1);
		int wybranaOpcja = rg.getCheckedRadioButtonId();
		RadioButton rb = (RadioButton) findViewById(wybranaOpcja);
		
		switch(widok.getId()){
		case R.id.button1:
			Toast toast1 = Toast.makeText(kontekst, "Wpisa�e�: " + pole1.getText().toString() + ". ", Toast.LENGTH_SHORT);
			toast1.show();
			break;
		case R.id.button2:
			Toast toast2 = Toast.makeText(kontekst, "Wybra�e� opcj�: " + rb.getText().toString() + ". ", Toast.LENGTH_SHORT);
			toast2.show();
			break;
		
    	case R.id.button3:
		    String tekst = "Podsumowanie: Wpisany tekst:" + pole1.getText().toString() + ". ";
		    tekst += "Wybrana opcja:" + rb.getText().toString() + ". ";
			Toast toast3 = Toast.makeText(kontekst, tekst, Toast.LENGTH_LONG);
			toast3.show();
    		break;
		}
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.relative_layout, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_relative_layout,
					container, false);
			return rootView;
		}
	}

}
