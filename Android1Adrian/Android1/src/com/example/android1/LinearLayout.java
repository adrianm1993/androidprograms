package com.example.android1;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RatingBar;
import android.widget.Toast;

public class LinearLayout extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_linear_layout);

		if (savedInstanceState == null) {
			getFragmentManager().beginTransaction()
					.add(R.id.container, new PlaceholderFragment()).commit();
		}
	}
	
	public void podsumowanie(View widok){
		switch(widok.getId()){
    	case R.id.podsumuj:
    		EditText imieINazwisko = (EditText) findViewById(R.id.editText1);
		    String tekst = "Podsumowanie: " + imieINazwisko.getText().toString() + ". ";
		    EditText wiek = (EditText) findViewById(R.id.editText2);
		    tekst += wiek.getText().toString() + ". ";
		    RadioButton radiobutton = (RadioButton)findViewById(R.id.radio1);
		    if(radiobutton.isChecked())
		    	tekst += "M�czyzna. ";
		    else 
		    	tekst += "Kobieta. ";
		    CheckBox checkbox = (CheckBox)findViewById(R.id.checkBox1);
		    if(checkbox.isChecked())
		    	tekst += "Zam�na/�onaty. ";
		    RatingBar ratingbar = (RatingBar)findViewById(R.id.ratingBar1);
		    tekst += " Ilos� gwiazdek:" + ratingbar.getRating();
		    
			Context kontekst = getApplicationContext();
			Toast toast = Toast.makeText(kontekst, tekst, Toast.LENGTH_LONG);
			toast.show();
    		break;
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.linear_layout, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_linear_layout,
					container, false);
			return rootView;
		}
	}

}
