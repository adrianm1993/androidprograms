package com.example.adrian.android2adrian;

        import android.app.Activity;
        import android.app.Fragment;
        import android.content.ComponentName;
        import android.content.Intent;
        import android.net.Uri;
        import android.os.Bundle;
        import android.provider.ContactsContract;
        import android.view.LayoutInflater;
        import android.view.Menu;
        import android.view.MenuItem;
        import android.view.View;
        import android.view.ViewGroup;
        import android.widget.Toast;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.container, new PlaceholderFragment()).commit();
        }
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    public void poKliknieciu(View view){

        switch(view.getId()){
            case R.id.button1:
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.setComponent(new ComponentName("com.example.android1","com.example.android1.MainActivity"));
                startActivity(intent);
                break;
            case R.id.button2:
                Intent dzwon = new Intent(Intent.ACTION_CALL, Uri.parse("tel:723-971-040"));
                startActivity(dzwon);
                break;
            case R.id.button3:
                Intent www = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.slaskwroclaw.pl"));
                startActivity(www);
                break;
            case R.id.button4:
                Intent wyslij = new Intent(Intent.ACTION_SENDTO, Uri.parse("sms:723971040"));
                wyslij.putExtra("sms_body", "Cześć tu ja");
                startActivity(wyslij);
                break;
            case R.id.button5:
                Intent numer = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:"));
                startActivity(numer);
                break;
            case R.id.button6:
                Intent galleryIntent = new Intent(Intent.ACTION_VIEW, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivity(galleryIntent);
                break;
            case R.id.button7:
                Intent kontanty = new Intent(Intent.ACTION_PICK);
                kontanty.setType(ContactsContract.Contacts.CONTENT_TYPE);
                startActivity(kontanty);
                break;
        }

    }

    @Override
    protected void onRestart() {
        //	super.onResume();
        super.onRestart();
        Toast toast = Toast.makeText(getApplicationContext(), R.string.powrot, Toast.LENGTH_SHORT);
        toast.show();
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        Toast toast = Toast.makeText(getApplicationContext(), R.string.koniec, Toast.LENGTH_SHORT);
        toast.show();
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container,
                    false);
            return rootView;
        }
    }

}
