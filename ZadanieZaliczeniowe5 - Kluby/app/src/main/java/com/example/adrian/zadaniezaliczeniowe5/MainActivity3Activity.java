package com.example.adrian.zadaniezaliczeniowe5;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ViewFlipper;


public class MainActivity3Activity extends Activity {
    ViewFlipper viewFlipper;
    float initialX=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_activity3);

        Button buttonBack = (Button)findViewById(R.id.button3);
        buttonBack.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                onBackPressed();
            }
        });

        Intent intent = getIntent();
        int position = intent.getIntExtra("itemPosition",0);

        viewFlipper = (ViewFlipper)findViewById(R.id.viewFlipper);
        viewFlipper.setDisplayedChild(position);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main_activity3, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onTouchEvent(MotionEvent touchevent){
        switch(touchevent.getAction()){
            case MotionEvent.ACTION_DOWN:
                initialX = touchevent.getX();
                break;
            case MotionEvent.ACTION_UP:
                float finalX = touchevent.getX();
                if(initialX > finalX) {
                    if (viewFlipper.getDisplayedChild() == 4)
                        break;
                    viewFlipper.setInAnimation(this, R.anim.in_right);
                    viewFlipper.setOutAnimation(this, R.anim.out_left);
                    viewFlipper.showNext();
                }
                else if(initialX < finalX){
                    if (viewFlipper.getDisplayedChild() == 0)
                        break;
                    viewFlipper.setInAnimation(this, R.anim.in_left);
                    viewFlipper.setOutAnimation(this, R.anim.out_right);
                    viewFlipper.showPrevious();
                }
                break;
        }
        return false;
    }

}
