package com.example.adrian.zadaniezaliczeniowe6;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.MediaController;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.VideoView;
import android.widget.ViewFlipper;


public class MainActivity4Activity extends Activity {
    public VideoView video;
    public ProgressDialog progressDialog;
    public MediaController mediaController;
    ViewFlipper viewFlipper;
    float initialX=0;
    private ImageButton play, pause, stop;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_activity4);

        Button buttonBack = (Button)findViewById(R.id.button3);
        buttonBack.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                onBackPressed();
            }
        });

        Intent intent = getIntent();
        int position = intent.getIntExtra("itemPosition",0);

        viewFlipper = (ViewFlipper)findViewById(R.id.viewFlipper);
        viewFlipper.setDisplayedChild(position);

        play = (ImageButton)findViewById(R.id.imageButton2);
        pause = (ImageButton)findViewById(R.id.imageButton);
        pause.setEnabled(false);
        stop = (ImageButton)findViewById(R.id.imageButton3);
        stop.setEnabled(false);
        playVideo();

    }

    public void playVideo()
    {
        if(mediaController == null)
            mediaController = new MediaController(MainActivity4Activity.this);

        switch(viewFlipper.getDisplayedChild()){
            case 0:
                video = (VideoView)findViewById(R.id.videoView);
                video.setVisibility(View.VISIBLE);
                viewFlipper.setVisibility(View.VISIBLE);
                video.setMediaController(mediaController);
                video.clearFocus();
                video.cancelLongPress();
                video.setVideoURI(Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.fakty));

                break;
            case 1:
                video = (VideoView)findViewById(R.id.videoView2);
                video.setVisibility(View.VISIBLE);
                video.setMediaController(mediaController);
                video.setVideoURI(Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.burza));
                break;
            case 2:
                video = (VideoView)findViewById(R.id.videoView3);
                video.setVisibility(View.VISIBLE);
                video.setMediaController(mediaController);
                video.setVideoURI(Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.teleexpress));
                break;
            case 3:
                video = (VideoView)findViewById(R.id.videoView4);
                video.setVisibility(View.VISIBLE);
                video.setMediaController(mediaController);
                video.setVideoURI(Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.panorama));
                break;
            case 4:
                video = (VideoView)findViewById(R.id.videoView5);
                video.setVisibility(View.VISIBLE);
                video.setMediaController(mediaController);
                video.setVideoURI(Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.wiadomosci));
                break;
        }
    }

    public void resetVideo(){
        video.pause();
        video.seekTo(0);
        pause.setEnabled(false);
        play.setEnabled(true);
        stop.setEnabled(false);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main_activity4, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onTouchEvent(MotionEvent touchevent){
        switch(touchevent.getAction()){
            case MotionEvent.ACTION_DOWN:
                initialX = touchevent.getX();
                break;
            case MotionEvent.ACTION_UP:
                float finalX = touchevent.getX();
                if(initialX > finalX && (Math.abs(initialX-finalX)>10)) {
                    if (viewFlipper.getDisplayedChild() == 4)
                        break;
                    viewFlipper.setInAnimation(this, R.anim.in_right);
                    viewFlipper.setOutAnimation(this, R.anim.out_left);
                    video.pause();
                    video.seekTo(0);
                    video.setVisibility(View.INVISIBLE);
                    viewFlipper.showNext();
                    resetVideo();
                    playVideo();

                }
                else if(initialX < finalX && (Math.abs(initialX-finalX)>10)){
                    if (viewFlipper.getDisplayedChild() == 0)
                        break;
                    viewFlipper.setInAnimation(this, R.anim.in_left);
                    viewFlipper.setOutAnimation(this, R.anim.out_right);
                    video.pause();
                    video.seekTo(0);
                    video.setVisibility(View.INVISIBLE);
                    viewFlipper.showPrevious();
                    resetVideo();
                    playVideo();
                }
                break;
        }
        return false;
    }

    public void play(View view){
        video.start();
        stop.setEnabled(true);
        play.setEnabled(false);
        pause.setEnabled(true);
    }

    public void pause(View view){
        video.pause();
        pause.setEnabled(false);
        stop.setEnabled(true);
        play.setEnabled(true);
    }

    public void reset(View view) {
        video.pause();
        video.seekTo(0);
        pause.setEnabled(false);
        play.setEnabled(true);
        stop.setEnabled(false);
    }


}
