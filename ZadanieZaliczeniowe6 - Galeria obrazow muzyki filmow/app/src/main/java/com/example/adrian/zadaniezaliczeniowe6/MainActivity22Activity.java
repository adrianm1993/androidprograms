package com.example.adrian.zadaniezaliczeniowe6;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;


public class MainActivity22Activity extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_activity22);

        Button buttonBack = (Button)findViewById(R.id.button3);
        buttonBack.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                onBackPressed();
            }
        });

        ExtendentViewPager mViewPager = (ExtendentViewPager) findViewById(R.id.view_pager);
        Intent intent = getIntent();
        int position = intent.getIntExtra("itemPosition",0);
        mViewPager.setAdapter(new TouchImageAdapter());
        mViewPager.setCurrentItem(position);
    }

    static class TouchImageAdapter extends PagerAdapter {

        private static int[] images = {R.drawable.slaskwroclaw, R.drawable.wislakrakow, R.drawable.lechiagdansk, R.drawable.lechpoznan, R.drawable.legiawarszawa};

        @Override
        public int getCount() {
            return images.length;
        }

        @Override
        public View instantiateItem(ViewGroup container, int position) {
            TouchImageView img = new TouchImageView(container.getContext());
            img.setImageResource(images[position]);
            img.resetZoom();
            container.addView(img, LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
            return img;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

    }
}