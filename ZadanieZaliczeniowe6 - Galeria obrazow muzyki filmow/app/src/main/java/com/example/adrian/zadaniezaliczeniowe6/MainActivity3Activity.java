package com.example.adrian.zadaniezaliczeniowe6;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ViewFlipper;


public class MainActivity3Activity extends Activity {
    ViewFlipper viewFlipper;
    float initialX=0;
    MediaPlayer mediaPlayer;
    private ImageButton play, pause, stop;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_activity3);

        Button buttonBack = (Button)findViewById(R.id.button3);
        buttonBack.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                mediaPlayer.pause();
                mediaPlayer.seekTo(0);
                onBackPressed();
            }
        });

        Intent intent = getIntent();
        int position = intent.getIntExtra("itemPosition",0);

        viewFlipper = (ViewFlipper)findViewById(R.id.viewFlipper);
        viewFlipper.setDisplayedChild(position);

        play = (ImageButton)findViewById(R.id.imageButton2);
        pause = (ImageButton)findViewById(R.id.imageButton);
        pause.setEnabled(false);
        stop = (ImageButton)findViewById(R.id.imageButton3);
        stop.setEnabled(false);
        playMusic();


    }

    public void playMusic(){
        switch(viewFlipper.getDisplayedChild()){
            case 0:
                mediaPlayer = MediaPlayer.create(this, R.raw.uefachampionsleague);
                break;
            case 1:
                mediaPlayer = MediaPlayer.create(this, R.raw.uefaeuropaeague);
                break;
            case 2:
                mediaPlayer = MediaPlayer.create(this, R.raw.fivbintro);
                break;
            case 3:
                mediaPlayer = MediaPlayer.create(this, R.raw.copaamerica);
                break;
            case 4:
                mediaPlayer = MediaPlayer.create(this, R.raw.confedcup);
                break;
        }
    }

    public void resetMusic(){
        mediaPlayer.pause();
        mediaPlayer.seekTo(0);
        pause.setEnabled(false);
        play.setEnabled(true);
        stop.setEnabled(false);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main_activity3, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onTouchEvent(MotionEvent touchevent){
        switch(touchevent.getAction()){
            case MotionEvent.ACTION_DOWN:
                initialX = touchevent.getX();
                break;
            case MotionEvent.ACTION_UP:
                float finalX = touchevent.getX();
                if(initialX > finalX && (Math.abs(initialX-finalX)>10)) {
                    if (viewFlipper.getDisplayedChild() == 4)
                        break;
                    viewFlipper.setInAnimation(this, R.anim.in_right);
                    viewFlipper.setOutAnimation(this, R.anim.out_left);
                    viewFlipper.showNext();
                    resetMusic();
                    playMusic();
                }
                else if(initialX < finalX && (Math.abs(initialX-finalX)>10)){
                    if (viewFlipper.getDisplayedChild() == 0)
                        break;
                    viewFlipper.setInAnimation(this, R.anim.in_left);
                    viewFlipper.setOutAnimation(this, R.anim.out_right);
                    viewFlipper.showPrevious();
                    resetMusic();
                    playMusic();
                }
                break;
        }
        return false;
    }

    public void play(View view){
        mediaPlayer.start();
        stop.setEnabled(true);
        play.setEnabled(false);
        pause.setEnabled(true);
    }

    public void pause(View view){
        mediaPlayer.pause();
        pause.setEnabled(false);
        stop.setEnabled(true);
        play.setEnabled(true);
    }

    public void reset(View view) {
        mediaPlayer.pause();
        mediaPlayer.seekTo(0);
        pause.setEnabled(false);
        play.setEnabled(true);
        stop.setEnabled(false);
    }

}
