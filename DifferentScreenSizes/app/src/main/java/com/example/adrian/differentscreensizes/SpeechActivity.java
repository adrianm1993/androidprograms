package com.example.adrian.differentscreensizes;

import android.app.ActionBar;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.speech.RecognizerIntent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import static android.view.Gravity.CENTER;
import static android.view.ViewGroup.LayoutParams.FILL_PARENT;
import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;

public class SpeechActivity extends AppCompatActivity {
    protected static final int RESULT_SPEECH = 1;

    ImageButton imageButton;
    TextView textView;
    Stadium stadium = new Stadium();
    List<Stadium> list = stadium.getDataForListView();
    TableLayout tableLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_speech);

        textView = (TextView) findViewById(R.id.textView5);
        createTable();
        button();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case RESULT_SPEECH: {
                if (resultCode == RESULT_OK && null != data) {
                    ArrayList<String> text = data
                            .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    textView.setText(text.get(0));
                    openDetails();
                }
                break;
            }

        }
    }

    private void openDetails(){
        for(int i=0; i<list.size(); i++){
            if(list.get(i).stadiumName.toUpperCase().equals(textView.getText().toString().toUpperCase())){
                Toast t = Toast.makeText(getApplicationContext(), list.get(i).stadiumName, Toast.LENGTH_SHORT);
                t.show();

                Intent intent = new Intent(this, Detail.class);
                intent.putExtra("itemPosition", i);
                startActivity(intent);
            }
        }
    }

    private void createTable(){
        tableLayout = (TableLayout) findViewById(R.id.tableLayout);

        for(int i=0; i<list.size(); i++){
            TableRow tableRow = new TableRow(this);
            TextView textView = new TextView(this);
            textView.setText(list.get(i).stadiumName);
            textView.setTextSize(15);
            textView.setTextColor(Color.BLACK);
            tableRow.addView(textView);
            tableLayout.addView(tableRow);
        }
    }

    private void button(){
        imageButton = (ImageButton) findViewById(R.id.imageButton);

        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, "pl-PL");

                try{
                    startActivityForResult(intent, RESULT_SPEECH);
                    textView.setText("");
                }
                catch (ActivityNotFoundException a){
                    Toast t = Toast.makeText(getApplicationContext(), "Twój telefon nie obsluguje tej funkcji :P", Toast.LENGTH_SHORT);
                    t.show();
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_speech, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
