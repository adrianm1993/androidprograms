package com.example.adrian.differentscreensizes;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import static com.example.adrian.differentscreensizes.R.layout.activity_main;

/**
 * Created by Adrian on 20.02.2016.
 */
public class Fragment1 extends Fragment {

    MyAdapter stadiumListAdapter;
    Stadium stadium = new Stadium();
    ListView list;

    public Fragment1() {
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        stadiumListAdapter = new MyAdapter();
        list = (ListView) getView().findViewById(R.id.listView);
        list.setAdapter(stadiumListAdapter);


        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Fragment fragment = getFragmentManager().findFragmentById(R.id.article_fragment);
                if (fragment != null && fragment.isVisible()) {

                    Intent intent = new Intent(getActivity(), MainActivity.class);
                    intent.putExtra("itemPosition", position);
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(getActivity(), Detail.class);
                    intent.putExtra("itemPosition", position);
                    startActivity(intent);
                }
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment1, container, false);
    }


    public class MyAdapter extends BaseAdapter {

        List<Stadium> stadiumList = stadium.getDataForListView();

        @Override
        public int getCount() {
            return stadiumList.size();
        }

        @Override
        public Stadium getItem(int position) {
            return stadiumList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            if(convertView==null) {
                LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.listitem, parent, false);
            }

            TextView stadiumName = (TextView)convertView.findViewById(R.id.textView);
            TextView stadiumDistance = (TextView)convertView.findViewById(R.id.textView2);
            ImageView imageView = (ImageView)convertView.findViewById(R.id.imageView);

            Stadium stadium = stadiumList.get(position);
            stadiumName.setText(stadium.stadiumName);
            stadiumDistance.setText(stadium.stadiumDistance);
            imageView.setImageResource(stadium.stadiumPhoto);

            return convertView;
        }

        public Stadium getStadium(int position) {
            return stadiumList.get(position);
        }
    }
}
