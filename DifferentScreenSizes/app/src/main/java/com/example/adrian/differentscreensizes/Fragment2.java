package com.example.adrian.differentscreensizes;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Adrian on 20.02.2016.
 */
public class Fragment2 extends Fragment {

    Stadium stadium = new Stadium();
    List<Stadium> stadiumList;
    Intent intent;
    TextView stadiumName, description;
    ImageView imageView;

    public Fragment2() {
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        stadiumList = stadium.getDataForListView();
        intent = getActivity().getIntent();
        int position = intent.getIntExtra("itemPosition",0);

        stadiumName = (TextView) getView().findViewById(R.id.textView3);
        description = (TextView) getView().findViewById(R.id.textView4);
        imageView = (ImageView) getView().findViewById(R.id.imageView2);

        stadiumName.setText(stadiumList.get(position).stadiumName);
        description.setText(stadiumList.get(position).stadiumDescription);
        imageView.setImageResource(stadiumList.get(position).stadiumPhoto);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        return inflater.inflate(R.layout.fragment2, container, false);
    }



}
