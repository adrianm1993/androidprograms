package com.example.adrian.zadaniezaliczeniowe2;

import java.util.ArrayList;
import java.util.List;

public class Stadium {
    String stadiumName;
    String stadiumDistance;
    String stadiumDescription;
    int stadiumPhoto;
    int stadiumPhoto2;
    int stadiumPhoto3;

    public List<Stadium> getDataForListView()
    {
        List<Stadium> dataList = new ArrayList<>();

        Stadium stadium = new Stadium();
        stadium.stadiumName = "Camp Nou";
        stadium.stadiumDistance = "2500km";
        stadium.stadiumDescription = "Camp Nou - stadion piłkarski w Barcelonie w Hiszpanii, na którym są rozgrywane mecze klubu FC Barcelona." +
                "Jego trybuny mieszczą 99 354 osób, co czyni go największym piłkarskim stadionem w Europie" +
                "Został wybudowany w latach 1954-1957.";
        stadium.stadiumPhoto = R.drawable.campnou;
        stadium.stadiumPhoto2 = R.drawable.campnou2;
        stadium.stadiumPhoto3 = R.drawable.campnou4;
        dataList.add(stadium);

        Stadium stadium2 = new Stadium();
        stadium2.stadiumName = "Stadion Wembley";
        stadium2.stadiumDistance = "2000km";
        stadium2.stadiumDescription = "Stadion Wembley - stadion sportowy znajdujący się w Londynie, w dzielnicy Wembley." +
                "Jest narodowym stadionem Anglii i powstał w miejsce Starego Wembley. " +
                "Jego pojemność to 90 tysięcy miejsc. Został wybudowany w latach 2003-2006.";
        stadium2.stadiumPhoto = R.drawable.wembley;
        stadium2.stadiumPhoto2 = R.drawable.wembley3;
        stadium2.stadiumPhoto3 = R.drawable.wembley4;
        dataList.add(stadium2);

        Stadium stadium3 = new Stadium();
        stadium3.stadiumName = "Estadio Santiago Bernabéu";
        stadium3.stadiumDistance = "2300km";
        stadium3.stadiumDescription = "Estadio Santiago Bernabéu – stadion piłkarski w stolicy Hiszpanii, Madrycie. " +
                "Jest macierzystym obiektem Realu Madryt, mecze rozgrywa na nim także reprezentacja Hiszpanii. " +
                "Jego pojemność to 85 254 miejsc.  22 maja 2010 roku odbył się tu 55. finał Ligi Mistrzów UEFA.";
        stadium3.stadiumPhoto = R.drawable.santiago;
        stadium3.stadiumPhoto2 = R.drawable.santiago1;
        stadium3.stadiumPhoto3 = R.drawable.santiago2;
        dataList.add(stadium3);

        Stadium stadium4 = new Stadium();
        stadium4.stadiumName = "Croke Park";
        stadium4.stadiumDistance = "2150km";
        stadium4.stadiumDescription = "Croke Park – stadion piłkarski w stolicy Irlandii, Dublinie.  Mecze na nim rozgrywa reprezentacja Irlandii." +
                "Jego pojemność to 82 300 miejsc. Został wybudowany w latach 1884-1913";
        stadium4.stadiumPhoto = R.drawable.croke;
        stadium4.stadiumPhoto2 = R.drawable.croke1;
        stadium4.stadiumPhoto3 = R.drawable.croke2;
        dataList.add(stadium4);

        Stadium stadium5 = new Stadium();
        stadium5.stadiumName = "Twickenham Stadium";
        stadium5.stadiumDistance = "2350km";
        stadium5.stadiumDescription = "Twickenham Stadium - stadion położony w hrabstwie Wielkiego Londynu w gminie Richmond upon Thames w dzielnicy Twickenham. " +
                "Jest to największy stadion do rugby w Wielkiej Brytanii, a jego pojemność wynosi aktualnie 82 000 miejsc." +
                "Został wybudowany w roku 1907.";
        stadium5.stadiumPhoto = R.drawable.twickenhm;
        stadium5.stadiumPhoto2 = R.drawable.twicken1;
        stadium5.stadiumPhoto3 = R.drawable.twicken4;
        dataList.add(stadium5);

        Stadium stadium6 = new Stadium();
        stadium6.stadiumName = "Stade de France";
        stadium6.stadiumDistance = "1900km";
        stadium6.stadiumDescription = "Stade de France - stadion sportowy na przedmieściach Paryża. " +
                "Narodowy stadion Francji o oficjalnej pojemności 81 338 miejsc siedzących, " +
                "na którym od 1998 swe najważniejsze mecze rozgrywają pierwsze reprezentacje Francji w piłce nożnej i rugby.";
        stadium6.stadiumPhoto = R.drawable.stade;
        stadium6.stadiumPhoto2 = R.drawable.stade1;
        stadium6.stadiumPhoto3 = R.drawable.stade2;
        dataList.add(stadium6);

        Stadium stadium7 = new Stadium();
        stadium7.stadiumName = "Signal Iduna Park";
        stadium7.stadiumDistance = "1000km";
        stadium7.stadiumDescription = "Signal Iduna Park -  stadion piłkarski w Dortmundzie, na którym rozgrywane są mecze klubu piłkarskiego Borussia Dortmund." +
                "Jego pojemność to 80 645 miejsc. Został wybudowany w latach 1971-1974.";
        stadium7.stadiumPhoto = R.drawable.signal;
        stadium7.stadiumPhoto2 = R.drawable.signal1;
        stadium7.stadiumPhoto3 = R.drawable.signal3;
        dataList.add(stadium7);

        Stadium stadium8 = new Stadium();
        stadium8.stadiumName = "Stadion Giuseppe Meazzy";
        stadium8.stadiumDistance = "1500km";
        stadium8.stadiumDescription = "Stadion Giuseppe Meazzy -  stadion piłkarski w Mediolanie. " +
                "Tradycyjny sektor kibiców Milanu to Zakole Południowe, a kibiców Interu – Zakole Północne." +
                "Jego pojemność to 80 018 miejsc. Został wybudowany w latach 1925-1926.";
        stadium8.stadiumPhoto = R.drawable.milano;
        stadium8.stadiumPhoto2 = R.drawable.sansiro1;
        stadium8.stadiumPhoto3 = R.drawable.sansiro2;
        dataList.add(stadium8);

        Stadium stadium9 = new Stadium();
        stadium9.stadiumName = "Stadion Łużniki";
        stadium9.stadiumDistance = "3500km";
        stadium9.stadiumDescription = "Łużniki – moskiewski stadion otwarty 31 lipca 1956, na którym rozgrywają swe mecze m.in. Spartak Moskwa i Torpedo Moskwa. " +
                "Nazwa obiektu pochodzi od terenu, który jest dość grząski, podmokły i bagienny." +
                "Jego pojemność to 78 360 miejsc.";
        stadium9.stadiumPhoto = R.drawable.luzniki;
        stadium9.stadiumPhoto2 = R.drawable.luzniki1;
        stadium9.stadiumPhoto3 = R.drawable.luzniki2;
        dataList.add(stadium9);

        Stadium stadium10 = new Stadium();
        stadium10.stadiumName = "Old Trafford";
        stadium10.stadiumDistance = "2450km";
        stadium10.stadiumDescription = "Old Trafford – stadion Manchesteru United, zwyczajowo nazwany Teatrem Marzeń. " +
                "Pojemność stadionu to 76 212 miejsc. Stadion został wybudowany w latach 1909-1910.";
        stadium10.stadiumPhoto = R.drawable.oldtrafford;
        stadium10.stadiumPhoto2 = R.drawable.old2;
        stadium10.stadiumPhoto3 = R.drawable.old3;
        dataList.add(stadium10);

        Stadium stadium11 = new Stadium();
        stadium11.stadiumName = "Stadion Narodowy w Warszawie";
        stadium11.stadiumDistance = "420km";
        stadium11.stadiumDescription = "Stadion Narodowy w Warszawie – wielofunkcyjny stadion sportowy (głównie piłkarski) znajdujący w Warszawie, " +
                "wybudowany w latach 2008-2011 w niecce byłego Stadionu Dziesięciolecia. " +
                "Jego pojemność to 58 500 miejsc.";
        stadium11.stadiumPhoto = R.drawable.narodowy;
        stadium11.stadiumPhoto2 = R.drawable.narodowy1;
        stadium11.stadiumPhoto3 = R.drawable.narodowy2;
        dataList.add(stadium11);

        Stadium stadium12 = new Stadium();
        stadium12.stadiumName = "Stadion Wrocław";
        stadium12.stadiumDistance = "10km";
        stadium12.stadiumDescription = "Stadion Miejski we Wrocławiu – stadion piłkarski we Wrocławiu, stanowiący własność miasta Wrocław. " +
                "Głównym użytkownikiem areny jest klub piłkarski Śląsk Wrocław." +
                "Pojemność stadionu wynosi 45 105. Stadion wybudowany w latach 2009-2011.";
        stadium12.stadiumPhoto = R.drawable.wroclaw;
        stadium12.stadiumPhoto2 = R.drawable.wroclaw1;
        stadium12.stadiumPhoto3 = R.drawable.wroclaw2;
        dataList.add(stadium12);

        return dataList;
    }
}
