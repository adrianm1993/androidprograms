package com.example.adrian.zadaniezaliczeniowe2;

        import android.app.Activity;
        import android.content.Intent;
        import android.os.Bundle;
        import android.view.Menu;
        import android.view.MenuItem;
        import android.view.MotionEvent;
        import android.view.View;
        import android.widget.Button;
        import android.widget.ImageView;
        import android.widget.RadioButton;
        import android.widget.TextView;
        import android.widget.ViewFlipper;

        import java.util.List;


public class OneStadium extends Activity {
    Stadium stadium = new Stadium();
    List<Stadium> stadiumList;
    Intent intent;
    Button buttonBack;
    TextView stadiumName, description;
    ImageView imageView, imageView2, imageView3;

    float initialX=0;
    ViewFlipper viewFlipper;
    RadioButton radiobutton1;
    RadioButton radiobutton2;
    RadioButton radiobutton3;
    TextView lewo;
    TextView prawo;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_one_stadium);

        stadiumList = stadium.getDataForListView();

        intent = getIntent();
        int position = intent.getIntExtra("itemPosition",0);

        setButtonBack();
        addElementsToView();
        setElementsVisibilityOnCreate(position);
    }

    private void setButtonBack(){
        buttonBack = (Button)findViewById(R.id.button);
        buttonBack.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void addElementsToView(){
        stadiumName = (TextView)findViewById(R.id.textView3);
        description = (TextView)findViewById(R.id.textView4);
        imageView = (ImageView)findViewById(R.id.imageView2);
        imageView2 = (ImageView)findViewById(R.id.imageView3);
        imageView3 = (ImageView)findViewById(R.id.imageView4);

        viewFlipper = (ViewFlipper)findViewById(R.id.flipper1);
        radiobutton1 = (RadioButton)findViewById(R.id.radioButton);
        radiobutton2 = (RadioButton)findViewById(R.id.radioButton2);
        radiobutton3 = (RadioButton)findViewById(R.id.radioButton3);

        lewo = (TextView)findViewById(R.id.textView5);
        prawo = (TextView)findViewById(R.id.textView6);
    }

    private void setElementsVisibilityOnCreate(int position){
        radiobutton1.setChecked(true);

        lewo.bringToFront();
        prawo.bringToFront();
        lewo.setVisibility(View.INVISIBLE);
        prawo.setVisibility(View.VISIBLE);

        stadiumName.setText(stadiumList.get(position).stadiumName);
        description.setText(stadiumList.get(position).stadiumDescription);
        imageView.setImageResource(stadiumList.get(position).stadiumPhoto);
        imageView2.setImageResource(stadiumList.get(position).stadiumPhoto2);
        imageView3.setImageResource(stadiumList.get(position).stadiumPhoto3);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_one_stadium, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onTouchEvent(MotionEvent touchevent){
        switch(touchevent.getAction()){
            case MotionEvent.ACTION_DOWN:
                initialX = touchevent.getX();
                break;
            case MotionEvent.ACTION_UP:
                float finalX = touchevent.getX();
                if(initialX > finalX) {
                    if (viewFlipper.getDisplayedChild() == 2)
                        break;
                    viewFlipper.setInAnimation(this, R.anim.in_right);
                    viewFlipper.setOutAnimation(this, R.anim.out_left);
                    viewFlipper.showNext();
                }
                else if(initialX < finalX){
                    if (viewFlipper.getDisplayedChild() == 0)
                        break;
                    viewFlipper.setInAnimation(this, R.anim.in_left);
                    viewFlipper.setOutAnimation(this, R.anim.out_right);
                    viewFlipper.showPrevious();
                }
                setRadio(viewFlipper.getDisplayedChild());
                break;
        }
        return false;
    }

    private void setRadio(int displayedChild) {
        switch (displayedChild) {
            case 0:
                radiobutton1.setChecked(true);
                radiobutton2.setChecked(false);
                radiobutton3.setChecked(false);
                lewo.setVisibility(View.INVISIBLE);
                prawo.setVisibility(View.VISIBLE);
                break;
            case 1:
                radiobutton1.setChecked(false);
                radiobutton2.setChecked(true);
                radiobutton3.setChecked(false);
                lewo.setVisibility(View.VISIBLE);
                prawo.setVisibility(View.VISIBLE);
                break;
            case 2:
                radiobutton1.setChecked(false);
                radiobutton2.setChecked(false);
                radiobutton3.setChecked(true);
                lewo.setVisibility(View.VISIBLE);
                prawo.setVisibility(View.INVISIBLE);
                break;
        }
    }

}

