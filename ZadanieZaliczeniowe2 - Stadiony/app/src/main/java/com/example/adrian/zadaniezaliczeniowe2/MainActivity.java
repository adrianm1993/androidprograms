package com.example.adrian.zadaniezaliczeniowe2;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;


import java.util.ArrayList;
import java.util.List;

import static android.widget.Toast.LENGTH_LONG;


public class MainActivity extends Activity {

    Stadium stadium = new Stadium();
    MyAdapter stadiumListAdapter;
    ListView list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        stadiumListAdapter = new MyAdapter();

        list = (ListView)findViewById(R.id.listView);
        list.setAdapter(stadiumListAdapter);
        list.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(MainActivity.this, OneStadium.class);
                intent.putExtra("itemPosition", position);
                startActivity(intent);
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public class MyAdapter extends BaseAdapter {

        List<Stadium> stadiumList = stadium.getDataForListView();

        @Override
        public int getCount() {
            return stadiumList.size();
        }

        @Override
        public Stadium getItem(int position) {
            return stadiumList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            if(convertView==null) {
                LayoutInflater inflater = (LayoutInflater) MainActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.listitem, parent, false);
            }

            TextView stadiumName = (TextView)convertView.findViewById(R.id.textView);
            TextView stadiumDistance = (TextView)convertView.findViewById(R.id.textView2);
            ImageView imageView = (ImageView)convertView.findViewById(R.id.imageView);

            Stadium stadium = stadiumList.get(position);
            stadiumName.setText(stadium.stadiumName);
            stadiumDistance.setText(stadium.stadiumDistance);
            imageView.setImageResource(stadium.stadiumPhoto);

            return convertView;
        }

        public Stadium getStadium(int position) {
            return stadiumList.get(position);
        }
    }

}
