package com.example.adrian.intents;

import android.app.SearchManager;
import android.content.ComponentName;
import android.content.Intent;
import android.net.Uri;
import android.provider.CalendarContract;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ActionMode;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.PopupMenu;

import java.util.Calendar;
import java.util.concurrent.Semaphore;

public class MainActivity extends AppCompatActivity {
    private Button button1, button2, button3, button4, button5, button6, button7, button8, button9, button10;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        createButton1();
        createButton2();
        createButton3();
        createButton4();
        createButton5();
        createButton6();
        createButton7();
        createButton8();
        createButton9();
        createButton10();
    }

    private void createButton10() {
        button10 = (Button) findViewById(R.id.button10);
        button10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPopup(v);
            }
        });
    }

    private void createButton9() {
        button9 = (Button) findViewById(R.id.button9);
        button9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent contacts = new Intent(Intent.ACTION_VIEW);
                contacts.setData(ContactsContract.Contacts.CONTENT_URI);
                startActivity(contacts);
            }
        });
    }

    private void createButton8() {
        button8 = (Button) findViewById(R.id.button8);
        button8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent event = new Intent(Intent.ACTION_INSERT, CalendarContract.Events.CONTENT_URI);
                event.putExtra(CalendarContract.EXTRA_EVENT_ALL_DAY, true);
                event.putExtra(CalendarContract.Events.TITLE, "Test");
                startActivity(event);
            }
        });
    }

    private void createButton7() {
        button7 = (Button) findViewById(R.id.button7);
        registerForContextMenu(button7);
    }

    private void createButton6() {
        button6 = (Button) findViewById(R.id.button6);
        button6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent takePhoto = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivity(takePhoto);
            }
        });
    }

    private void createButton5() {
        button5 = (Button) findViewById(R.id.button5);
        button5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent call = new Intent(Intent.ACTION_CALL, Uri.parse("tel:723971040"));
                startActivity(call);
            }
        });
    }

    private void createButton4() {
        button4 = (Button) findViewById(R.id.button4);
        registerForContextMenu(button4);
        button4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActionMode(mActionModeCallBack);
            }
        });
    }

    private void createButton3() {
        button3 = (Button) findViewById(R.id.button3);
        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent map = new Intent(Intent.ACTION_VIEW, Uri.parse("geo:51.055713, 17.066062"));
                startActivity(map);
            }
        });
    }

    private void createButton2() {
        button2 = (Button) findViewById(R.id.button2);
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent dial = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:723971040"));
                startActivity(dial);
            }
        });
    }

    private void createButton1(){
        button1 = (Button) findViewById(R.id.button);
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent newApp = new Intent(Intent.ACTION_MAIN);
                newApp.setComponent(new ComponentName("com.example.adrian.swipeview", "com.example.adrian.swipeview.MainActivity"));
                startActivity(newApp);
            }
        });
    }

/// MENU KONTEKSTOWE NA DWA SPOSOBY DLA BUTTONA4 ///
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        if(v.getId() == R.id.button4){
            MenuInflater inflater = getMenuInflater();
            inflater.inflate(R.menu.menu_contextual, menu);
        }
        else if (v.getId() == R.id.button7){
            MenuInflater inflater = getMenuInflater();
            inflater.inflate(R.menu.menu_group, menu);
        }

    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.contextual_website:
                Intent www = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.onet.pl"));
                startActivity(www);
                break;
            case R.id.contextual_search:
                Intent search = new Intent(Intent.ACTION_WEB_SEARCH);
                search.putExtra(SearchManager.QUERY, "wrocław");
                startActivity(search);
                break;
            case R.id.group_email:
                Intent email = new Intent(Intent.ACTION_SEND);
                email.setType("text/plain");
                email.putExtra(Intent.EXTRA_EMAIL, new String[]{"apmalinowski@gmail.com"});
                email.putExtra(Intent.EXTRA_SUBJECT, "Test");
                email.putExtra(Intent.EXTRA_TEXT, "To jest test");
                email.putExtra(Intent.EXTRA_STREAM, Uri.parse("content://path/to/email/attachment"));
                startActivity(email);
                break;
            case R.id.group_sms:
                Intent send = new Intent(Intent.ACTION_SEND);
                send.putExtra(Intent.EXTRA_TEXT, "To jest testowy sms");
                send.setType("text/plain");
                startActivity(send);
                break;
            case R.id.group_sms_to:
                Intent sendTo = new Intent(Intent.ACTION_SENDTO);
                sendTo.setData(Uri.parse("smsto:723971040"));
                sendTo.putExtra("sms_body", "To jest testowy sms");
                startActivity(sendTo);
                break;
        }
        return super.onContextItemSelected(item);
    }

    private ActionMode.Callback mActionModeCallBack = new ActionMode.Callback() {
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            MenuInflater inflater = mode.getMenuInflater();
            inflater.inflate(R.menu.menu_contextual, menu);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch(item.getItemId()){
                case R.id.contextual_website:
                    Intent www = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.onet.pl"));
                    startActivity(www);
                    mode.finish();
                    break;
                case R.id.contextual_search:
                    Intent search = new Intent(Intent.ACTION_WEB_SEARCH);
                    search.putExtra(SearchManager.QUERY, "wrocław");
                    startActivity(search);
                    mode.finish();
                    break;
            }
            return false;
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {

        }
    };
///-----------------------------------------------///


/// MENU OPCJI ///
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        switch(item.getItemId()){
            case R.id.another_app:
                Intent newApp = new Intent(Intent.ACTION_MAIN);
                newApp.setComponent(new ComponentName("com.example.adrian.swipeview", "com.example.adrian.swipeview.MainActivity"));
                startActivity(newApp);
                break;
            case R.id.phone:
                Intent dial = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:723971040"));
                startActivity(dial);
                break;
            case R.id.map:
                Intent map = new Intent(Intent.ACTION_VIEW, Uri.parse("geo:51.055713, 17.066062"));
                startActivity(map);
                break;
            case R.id.call:
                Intent call = new Intent(Intent.ACTION_CALL, Uri.parse("tel:723971040"));
                startActivity(call);
                break;
            case R.id.camera:
                Intent takePhoto = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivity(takePhoto);
                break;
            case R.id.contacts:
                Intent contacts = new Intent(Intent.ACTION_VIEW);
                contacts.setData(ContactsContract.Contacts.CONTENT_URI);
                startActivity(contacts);
                break;
            case R.id.event:
                Intent event = new Intent(Intent.ACTION_INSERT, CalendarContract.Events.CONTENT_URI);
                event.putExtra(CalendarContract.EXTRA_EVENT_ALL_DAY, true);
                event.putExtra(CalendarContract.Events.TITLE, "Test");
                startActivity(event);
                break;
        }

        return super.onOptionsItemSelected(item);
    }
///-----------------------------------------------///


/// POPUP MENU ///
    public void showPopup(View view){
        PopupMenu popup = new PopupMenu(this, view);
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.popup_photos:
                        Intent photos = new Intent(Intent.ACTION_VIEW);
                        photos.setData(MediaStore.Images.Media.INTERNAL_CONTENT_URI);
                        startActivity(photos);
                        return true;
                    case R.id.popup_videos:
                        Intent videos = new Intent(Intent.ACTION_VIEW);
                        videos.setData(MediaStore.Video.Media.INTERNAL_CONTENT_URI);
                        startActivity(videos);
                        return true;
                    default:
                        return false;
                }
            }
        });
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.menu_popup, popup.getMenu());
        popup.show();
    }
///----------------------------------------------///


}
