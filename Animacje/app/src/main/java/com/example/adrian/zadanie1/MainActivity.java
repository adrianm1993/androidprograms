package com.example.adrian.zadanie1;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends Activity {
    ImageButton button1, button2, button3, button4;
    TextView textView1, textView2,textView3, textView4, textView5, textView6,textView7, textView8, textView9, textView10, textView11, textView12, textView13;
    ImageView imageView1, imageView2, imageView3, imageView4, imageView5, imageView6, imageView7, imageView8, imageView9;
    Animation animation1, animation2, animation3, animation6, animation7, animation8, animation9;
    AnimationDrawable firstAnimation, secondAnimation, thirdAnimation, fourthAnimation, fifthAnimation;
    AlertDialog alertDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        addButtons();
        addTextViews();
        addImages();
        addAnimations();
        setImagesAnimation();
        setAlerts();
        setVisibilityOfObjects();
        addAnimationsListener();
        addButtonsListener();
    }

    private void addButtons(){
        button1 = (ImageButton) findViewById(R.id.imageButton);
        button2 = (ImageButton) findViewById(R.id.imageButton2);
        button3 = (ImageButton) findViewById(R.id.imageButton3);
        button4 = (ImageButton) findViewById(R.id.imageButton4);
    }

    private void addImages(){
        imageView1 = (ImageView) findViewById(R.id.imageView);
        imageView2 = (ImageView) findViewById(R.id.imageView2);
        imageView3 = (ImageView) findViewById(R.id.imageView3);
        imageView4 = (ImageView) findViewById(R.id.imageView4);
        imageView5 = (ImageView) findViewById(R.id.imageView5);
        imageView6 = (ImageView) findViewById(R.id.imageView6);
        imageView7 = (ImageView) findViewById(R.id.imageView7);
        imageView8 = (ImageView) findViewById(R.id.imageView8);
        imageView9 = (ImageView) findViewById(R.id.imageView9);
    }

    private void addTextViews(){
        textView1 = (TextView) findViewById(R.id.textView);
        textView2 = (TextView) findViewById(R.id.textView2);
        textView3 = (TextView) findViewById(R.id.textView3);
        textView4 = (TextView) findViewById(R.id.textView4);
        textView5 = (TextView) findViewById(R.id.textView5);
        textView6 = (TextView) findViewById(R.id.textView6);
        textView7 = (TextView) findViewById(R.id.textView7);
        textView8 = (TextView) findViewById(R.id.textView8);
        textView9 = (TextView) findViewById(R.id.textView9);
        textView10 = (TextView) findViewById(R.id.textView10);
        textView11 = (TextView) findViewById(R.id.textView11);
        textView12 = (TextView) findViewById(R.id.textView12);
        textView13 = (TextView) findViewById(R.id.textView13);
    }

    private void addAnimations(){
        animation1 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.move_left_right);
        animation2 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.move_right_left);
        animation3 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.move_up_down);
        animation6 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.bird_photo);
        animation7 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.forest_photo);
        animation8 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.flower_photo);
        animation9 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.tree_photo);
    }

    private void setVisibilityOfObjects(){
        imageView2.setVisibility(View.INVISIBLE);
        imageView3.setVisibility(View.INVISIBLE);
        imageView4.setVisibility(View.INVISIBLE);
        imageView5.setVisibility(View.INVISIBLE);
        textView7.bringToFront();
        textView8.bringToFront();
        textView9.bringToFront();
        textView1.bringToFront();
        textView3.bringToFront();
        textView4.bringToFront();
        textView5.bringToFront();
        textView1.setVisibility(View.INVISIBLE);
        textView3.setVisibility(View.INVISIBLE);
        textView4.setVisibility(View.INVISIBLE);
        textView5.setVisibility(View.INVISIBLE);
        textView10.setVisibility(View.INVISIBLE);
        textView11.setVisibility(View.INVISIBLE);
        textView12.setVisibility(View.INVISIBLE);
        textView13.setVisibility(View.INVISIBLE);
    }

    private void addAnimationsListener(){
        animation1.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                imageView1.setVisibility(View.INVISIBLE);
                textView6.setVisibility(View.INVISIBLE);
                textView1.setVisibility(View.VISIBLE);
                imageView5.setVisibility(View.VISIBLE);
                textView1.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                imageView7.startAnimation(animation7);
                imageView2.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });

        animation2.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                imageView4.setVisibility(View.INVISIBLE);
                textView9.setVisibility(View.VISIBLE);
                textView5.setVisibility(View.VISIBLE);
                textView9.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                imageView9.startAnimation(animation9);
                imageView3.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });

        animation3.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                imageView2.setVisibility(View.INVISIBLE);
                textView3.setVisibility(View.VISIBLE);
                textView7.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                imageView6.startAnimation(animation6);
                imageView4.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
    }

    private void addButtonsListener(){
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (secondAnimation.isRunning()) {
                    imageView5.startAnimation(animation3);
                }
            }
        });
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (thirdAnimation.isRunning()) {
                    thirdAnimation.stop();
                    imageView5.clearAnimation();
                    imageView3.setVisibility(View.INVISIBLE);
                    textView8.setVisibility(View.VISIBLE);
                    textView4.setVisibility(View.VISIBLE);
                    imageView8.startAnimation(animation8);
                    imageView5.setVisibility(View.INVISIBLE);
                    textView8.setVisibility(View.INVISIBLE);
                    alertDialog.show();
                }

            }
        });
        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (firstAnimation.isRunning()) {
                    imageView5.startAnimation(animation1);
                }
            }
        });
        button4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (fourthAnimation.isRunning()) {
                    imageView5.startAnimation(animation2);
                }
            }
        });
    }

    private void setImagesAnimation(){
        imageView1.setBackgroundResource(R.drawable.animation);
        firstAnimation = (AnimationDrawable) imageView1.getBackground();
        firstAnimation.start();

        imageView2.setBackgroundResource(R.drawable.animation);
        secondAnimation = (AnimationDrawable) imageView2.getBackground();
        secondAnimation.start();

        imageView3.setBackgroundResource(R.drawable.animation);
        thirdAnimation = (AnimationDrawable) imageView3.getBackground();
        thirdAnimation.start();

        imageView4.setBackgroundResource(R.drawable.animation);
        fourthAnimation = (AnimationDrawable) imageView4.getBackground();
        fourthAnimation.start();

        imageView5.setBackgroundResource(R.drawable.animation);
        fifthAnimation = (AnimationDrawable) imageView5.getBackground();
        fifthAnimation.start();
    }

    private void setAlerts(){
        alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setMessage("Czy chcesz jeszcze raz zagrać?");
        alertDialog.setButton(-1, "OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent i = getBaseContext().getPackageManager()
                        .getLaunchIntentForPackage(getBaseContext().getPackageName());
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            }
        });
        alertDialog.setButton(-3, "Nie", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
