package com.example.adrian.zadaniezaliczeniowe4;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;


public class MainActivity extends Activity {
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void open(View view){
        switch(view.getId()){
            case R.id.imageView:
                intent = new Intent(MainActivity.this,Player.class);
                intent.putExtra("numberOfMusic",1);
                startActivity(intent);
                break;
            case R.id.imageView4:
                intent = new Intent(MainActivity.this,Player.class);
                intent.putExtra("numberOfMusic",2);
                startActivity(intent);
                break;
            case R.id.imageView5:
                intent = new Intent(MainActivity.this,Player.class);
                intent.putExtra("numberOfMusic",3);
                startActivity(intent);
                break;
            case R.id.imageView6:
                intent = new Intent(MainActivity.this,Player.class);
                intent.putExtra("numberOfMusic",4);
                startActivity(intent);
                break;
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
