package com.example.adrian.zadaniezaliczeniowe4;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Environment;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;

import java.io.IOException;


public class Player extends Activity {
    MediaPlayer mediaPlayer;
    private ImageButton play, pause, stop;
    ImageView imageView;
    Button back;
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player);

        intent = getIntent();
        int position = intent.getIntExtra("numberOfMusic",0);

        createButtonBack();
        addElementsToView();
        onClick(position);
    }

    public void play(View view){
        mediaPlayer.start();
        stop.setEnabled(true);
        play.setEnabled(false);
        pause.setEnabled(true);
    }

    public void pause(View view){
        mediaPlayer.pause();
        pause.setEnabled(false);
        stop.setEnabled(true);
        play.setEnabled(true);
    }

    public void reset(View view) {
        mediaPlayer.pause();
        mediaPlayer.seekTo(0);
        pause.setEnabled(false);
        play.setEnabled(true);
        stop.setEnabled(false);
    }

    private void addElementsToView(){
        play = (ImageButton)findViewById(R.id.imageButton2);
        pause = (ImageButton)findViewById(R.id.imageButton);
        pause.setEnabled(false);
        stop = (ImageButton)findViewById(R.id.imageButton3);
        stop.setEnabled(false);
        imageView = (ImageView)findViewById(R.id.imageView3);
    }

    private void createButtonBack(){
        back = (Button)findViewById(R.id.button);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Player.this, MainActivity.class);
                startActivity(intent);
                mediaPlayer.pause();
                mediaPlayer.seekTo(0);
            }
        });
    }

    private void onClick(int position){
        switch(position){
            case 1:
                imageView.setImageResource(R.drawable.lm);
                mediaPlayer = MediaPlayer.create(this, R.raw.uefachampionsleague);
                break;
            case 2:
                imageView.setImageResource(R.drawable.le);
                mediaPlayer = MediaPlayer.create(this, R.raw.uefaeuropaeague);
                break;
            case 3:
                imageView.setImageResource(R.drawable.spain);
                mediaPlayer = MediaPlayer.create(this, R.raw.spainsong);
                break;
            case 4:
                imageView.setImageResource(R.drawable.slask);
                mediaPlayer = MediaPlayer.create(this, R.raw.funhouse);
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_player, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
