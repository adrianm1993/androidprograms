package com.example.adrian.android3adrian;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends Activity {

    public static final int poz1 = 1;
    public static final int poz2 = 2;
    public static final int poz5 = 5;
    public static final int poz6 = 6;
    public static final int poz7 = 7;
    public static final int menuKontekstowePoz1 = 1;
    public static final int menuKontekstowePoz2 = 2;
    public static final int menuKontekstowePoz3 = 3;
    public static final int menuKontekstowePoz4 = 4;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.container, new PlaceholderFragment()).commit();
        }

        EditText edittext1 = (EditText) findViewById(R.id.editText1);
        EditText edittext2 = (EditText) findViewById(R.id.editText2);
        registerForContextMenu(edittext1);
        registerForContextMenu(edittext2);

        Button przycisk = (Button)findViewById(R.id.button1);
        przycisk.setOnClickListener(new OnClickListener(){
            @Override
            public void onClick(View arg0) {
                Intent intent = new Intent(getApplicationContext(), ActionBar.class);
                startActivity(intent);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        super.onCreateOptionsMenu(menu);
        menu.add(0, poz1, 1, "Kolor niebieski").setIcon(R.drawable.niebieski);
        menu.add(0, poz2, 2, "Kolor czerwony").setIcon(R.drawable.czerwony);

        SubMenu sm = menu.addSubMenu(0, 4, 4, "Wielkości czcionek").setIcon(R.drawable.czcionka);
        sm.add(0,poz5,1,"Czcionka 12").setIcon(R.drawable.ic_launcher);
        sm.add(0,poz6,2,"Czcionka 20").setIcon(R.drawable.ic_launcher);
        sm.add(0,poz7,3,"Czcionka 24").setIcon(R.drawable.ic_launcher);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }


        EditText edittext1 = (EditText) findViewById(R.id.editText1);
        EditText edittext2 = (EditText) findViewById(R.id.editText2);

        switch(item.getItemId()){
            case poz1:
                //getResources().getColor(android.R.color.black)
                edittext1.setTextColor(Color.BLUE);
                edittext2.setTextColor(Color.BLUE);
                break;
            case poz2:
                edittext1.setTextColor(Color.RED);
                edittext2.setTextColor(Color.RED);
                break;
            case R.id.poz3:
                edittext1.setTextColor(Color.GREEN);
                edittext2.setTextColor(Color.GREEN);
                break;
            case poz5:
                edittext1.setTextSize(12);
                edittext2.setTextSize(12);
                break;
            case poz6:
                edittext1.setTextSize(20);
                edittext2.setTextSize(20);
                break;
            case poz7:
                edittext1.setTextSize(24);
                edittext2.setTextSize(24);
                break;
        }


        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onCreateContextMenu(ContextMenu cm, View v, ContextMenuInfo cmi){
        super.onCreateContextMenu(cm, v, cmi);

        if(v.getId()==R.id.editText1){
            cm.setHeaderTitle("Menu konkekstowe dla pierwszego okna");
            cm.add(0, menuKontekstowePoz1, 1, "Kolor jasnoniebieski");
            cm.add(0, menuKontekstowePoz2, 2, "Kolor żółty");
            cm.add(0, menuKontekstowePoz3, 3, "Kolor szary jasny");
            cm.add(0, menuKontekstowePoz4, 4, "Czcionka 16");
        }
        if(v.getId()==R.id.editText2){
            cm.setHeaderTitle("Menu konkekstowe dla drugiego okna");
//			cm.add(0, menuKontekstowePoz5, 1, "Wielkość1");
//			cm.add(0, menuKontekstowePoz6, 2, "Wielkość2");
//			cm.add(0, menuKontekstowePoz7, 3, "Wielkość3");
//			cm.add(0, menuKontekstowePoz8, 4, "Kolor1");
            getMenuInflater().inflate(R.menu.menukontekstowe2, cm);
        }

    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        EditText edittext1 = (EditText) findViewById(R.id.editText1);
        EditText edittext2 = (EditText) findViewById(R.id.editText2);

        switch(item.getItemId()){
            case menuKontekstowePoz1:
                edittext1.setTextColor(Color.CYAN);
                break;
            case menuKontekstowePoz2:
                edittext1.setTextColor(Color.YELLOW);
                break;
            case menuKontekstowePoz3:
                edittext1.setTextColor(Color.LTGRAY);
                break;
            case menuKontekstowePoz4:
                edittext1.setTextSize(16);
                break;
        }

        switch(item.getItemId()){
            case R.id.item01:
                edittext2.setTextSize(15);
                break;
            case R.id.item02:
                edittext2.setTextSize(17);
                break;
            case R.id.item03:
                edittext2.setTextSize(19);
                break;
            case R.id.item04:
                edittext2.setTextColor(Color.DKGRAY);
                break;
        }

        return true;
    }



    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container,
                    false);
            return rootView;
        }
    }

}