package com.example.adrian.android3adrian;

import android.app.Activity;
import android.app.Fragment;
import android.graphics.Color;
import android.os.Bundle;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

public class ActionBar extends Activity {
    public static final int menuKontekstowePoz1 = 1;
    public static final int menuKontekstowePoz2 = 2;
    public static final int menuKontekstowePoz3 = 3;
    public static final int menuKontekstowePoz4 = 4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_action_bar);

        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.container, new PlaceholderFragment()).commit();
        }
    //    getActionBar().setDisplayHomeAsUpEnabled(true);


        View view1 = findViewById(R.id.editText1);
        view1.setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(View view1) {
                ActionBar.this.startActionMode(mActionModeCallback1);
                view1.setSelected(true);
                return true;
            }
        });

        View view2 = findViewById(R.id.editText2);
        view2.setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(View view2) {
                ActionBar.this.startActionMode(mActionModeCallback2);
                view2.setSelected(true);
                return true;
            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_action_bar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }

        EditText edittext1 = (EditText) findViewById(R.id.editText1);
        EditText edittext2 = (EditText) findViewById(R.id.editText2);
        float pom1 = edittext1.getTextSize();
        float pom2 = edittext2.getTextSize();
        float pom3 = 6;

        switch(item.getItemId()){
            case R.id.poz1:
                edittext1.setTextColor(Color.YELLOW);
                edittext2.setTextColor(Color.YELLOW);
                break;
            case R.id.poz2:
                edittext1.setTextColor(Color.RED);
                edittext2.setTextColor(Color.RED);
                break;
            case R.id.poz3:
                edittext1.setTextColor(Color.GREEN);
                edittext2.setTextColor(Color.GREEN);
                break;
            case R.id.poz4:
                edittext1.setTextSize(0, pom1-pom3);
                edittext2.setTextSize(0, pom2-pom3);
                break;
            case R.id.poz5:
                edittext1.setTextSize(0, pom1+pom3);
                edittext2.setTextSize(0, pom2+pom3);
                break;
        }

        return super.onOptionsItemSelected(item);
    }



    private ActionMode.Callback mActionModeCallback2 = new ActionMode.Callback() {
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            getMenuInflater().inflate(R.menu.menukontekstowe2, menu);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            EditText edittext2 = (EditText) findViewById(R.id.editText2);

            switch(item.getItemId()){
                case R.id.item01:
                    edittext2.setTextSize(15);
                    break;
                case R.id.item02:
                    edittext2.setTextSize(17);
                    break;
                case R.id.item03:
                    edittext2.setTextSize(19);
                    break;
                case R.id.item04:
                    edittext2.setTextColor(Color.DKGRAY);
                    break;
            }
            return true;
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
        }
    };


    private ActionMode.Callback mActionModeCallback1 = new ActionMode.Callback() {
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            menu.add(0, menuKontekstowePoz1, 1, "Kolor jasnoniebieski");
            menu.add(0, menuKontekstowePoz2, 2, "Kolor żółty");
            menu.add(0, menuKontekstowePoz3, 3, "Kolor szary jasny");
            menu.add(0, menuKontekstowePoz4, 4, "Czcionka 16");
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            EditText edittext1 = (EditText) findViewById(R.id.editText1);

            switch(item.getItemId()){
                case menuKontekstowePoz1:
                    edittext1.setTextColor(Color.CYAN);
                    break;
                case menuKontekstowePoz2:
                    edittext1.setTextColor(Color.YELLOW);
                    break;
                case menuKontekstowePoz3:
                    edittext1.setTextColor(Color.LTGRAY);
                    break;
                case menuKontekstowePoz4:
                    edittext1.setTextSize(16);
                    break;
            }
            return true;
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
        }
    };



    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_action_bar,
                    container, false);
            return rootView;
        }
    }

}
